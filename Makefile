info: header
define HEADER
      ___           __  ___                           
|    |__      |\/| /  \  |                            
|___ |___     |  | \__/  |                            
                                                      
 __     __        __             ___    __        ___ 
|  \ | |__) |    /  \  |\/|  /\   |  | /  \ |  | |__  
|__/ | |    |___ \__/  |  | /~~\  |  | \__X \__/ |___ 

endef
export HEADER

bootstrap:  ## Initialisation du projet
	cp --no-clobber .env.tpl .env
	./app/manage.py bootstrap

clearcache: ## Vidage du cache django
	./app/manage.py clearcache

dev: ## Lancer le serveur de dev (avec CLI tailwind)
	./app/manage.py devserver

fetch: ## récupérer des articles sur le diplo, arguments avec args="2022"
	./app/manage.py fetch $(args)

extract: ## extraire le contenu des articles, arguments avec args="2022"
	./app/manage.py extract $(args)

analyze: ## analyser le contenu des articles, arguments avec args="2022"
	./app/manage.py analyze $(args)

load: ## charger les mots en base, arguments avec args="2022"
	./app/manage.py load $(args)

import: ## fetch + extract + analyze + load
	./app/manage.py import $(args)

update: ## notifier l'instance de prod de la mise à dispo de nouvelles données
	./app/manage.py update $(args)

lint:  ## Linting et validations
	python -m ruff format --diff app
	python -m djhtml app --check
	python -m ruff check app
	python -m mypy app

tests:  ## Tests unitaires, arguments pytest avec args="--lf"
	python -m pytest $(args)

requirements: ## Maj des dependances puis syncho de l'environnement
	python -m piptools compile --upgrade --resolver backtracking --verbose \
		--output-file=requirements.txt pyproject.toml
	python -m piptools compile --upgrade --resolver backtracking --verbose \
		--extra=dev --output-file=requirements.dev.txt pyproject.toml
	python -m piptools compile --upgrade --resolver backtracking --verbose \
		--extra=analyze --output-file=requirements.analyze.txt pyproject.toml
	python -m piptools sync requirements.dev.txt

run: ## Lancer le serveur
	./app/manage.py runserver

shell: ## shell django
	./app/manage.py shell

manage: ## manage.py + arguments passés avec args, ex. args="makemigrations xxx"
	./app/manage.py $(args)

install-tailwind:  ## Récupérer la CLI Tailwind
	curl -sLO https://github.com/tailwindlabs/tailwindcss/releases/latest/download/tailwindcss-linux-x64
	chmod +x tailwindcss-linux-x64
	mv tailwindcss-linux-x64 tailwindcss

install-spacy:  ## Installation des modèles spaCy
	python -m spacy download fr_core_news_lg
	

.PHONY: all

help:
	@echo "$$HEADER"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
