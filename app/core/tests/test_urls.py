import pytest


@pytest.mark.django_db
class TestAdminPath:
    def test_django_default(self, admin_client):
        response = admin_client.get("/admin/")
        assert response.status_code == 404

    def test_default_setting(self, admin_client):
        response = admin_client.get("/gestion/")
        assert response.status_code == 200
