"""Outils."""

import subprocess
from datetime import datetime
from secrets import compare_digest
from typing import TYPE_CHECKING
from zipfile import ZipFile

import requests
from django.conf import settings
from django.core.exceptions import PermissionDenied, ValidationError
from django.core.management import call_command

if TYPE_CHECKING:
    from django.http import HttpRequest

MIN_YEAR = 1954


def restart_service() -> None:
    """Relance le service systemd."""
    subprocess.call(  # noqa: S603
        ["sudo", "systemctl", "restart", "motdiplo"],  # noqa: S607
        shell=False,
    )


def validate_gitlab_token(request: "HttpRequest") -> None:
    """Vérification de l'entête `X-Gitlab-Token`.

    Raises:
        PermissionDenied si le jeton ne correspond pas à celui renseigné dans les
        settings
    """
    token = request.headers.get("X-Gitlab-Token", "")
    if not compare_digest(token, settings.GITLAB_TOKEN):
        raise PermissionDenied()


def validate_year_month(request: "HttpRequest") -> tuple[str, str]:
    """Validation de l'année & mois envoyés par GitLab.

    Premier numéro du diplo : septembre 1954
    Dernier numéro : _mois en cours_ _année en cours_

    Args:
        request: la requête HTTP

    Raises:
        ValidationError: si l'année ou le mois ne correspondent pas à des valeurs
        valides

    Returns:
        Une paire année, mois
    """
    messages: dict[str, str] = {}
    try:
        year = request.POST["year"]
        month = request.POST["month"]
        if not MIN_YEAR <= int(year) <= datetime.now().year:
            messages["year"] = "invalide"
        min_month = 9 if int(year) == MIN_YEAR else 1
        max_month = datetime.now().month if int(year) == datetime.now().year else 12
        if not min_month <= int(month) <= max_month:
            messages["month"] = "invalide"
    except Exception as error:
        raise ValidationError("Année ou mois manquant", code="missing") from error

    if messages:
        raise ValidationError(messages, code="invalid")
    return (year, month)


def validate_job_id(request: "HttpRequest") -> int:
    """Validation d'un job_id.

    Args:
        request: la requête HTTP

    Raises:
        ValidationError: si ça n'est pas un entier ou s'il est manquant

    Returns:
        un entier
    """
    try:
        return int(request.POST["job_id"])
    except KeyError as error:
        raise ValidationError("Job id manquant", code="missing") from error
    except Exception as error:
        raise ValidationError("Job id invalide", code="invalid") from error


def validate_update(file_list: list[str], year: str, month: str) -> None:
    """Validation du contenu de l'archive de mise à jour.

    Le nom des fichiers doit
    - commencer par archives/year/month et se terminer par csv, txt ou html
    - ou être `archives/`, `archives/year`, `archives/year/month`

    Args:
        file_list: la liste des noms des fichiers de l'archive (ZipFile.namelist)
        year: l'année ciblée
        month: le mois ciblé

    Raises:
        ValidationError: si un fichier de la mise à jour n'est pas autorisé
    """
    messages: dict[str, str] = {}
    valid_files_prefix = f"archives/{year}/{month}/"
    valid_folders = ["archives/", f"archives/{year}/", valid_files_prefix]
    valid_files_suffix = (".txt", ".html", ".csv")
    for file_name in file_list:
        if file_name in valid_folders:
            continue
        if file_name.startswith(valid_files_prefix) and file_name.endswith(
            valid_files_suffix
        ):
            continue
        messages[file_name] = "Fichier invalide dans la mise à jour"
    if messages:
        raise ValidationError(messages)


def download_update(year: str, month: str) -> None:
    """Téléchargement des artefacts de mise à jour.

    Télécharge les artefacts du job update-db sur la branche principale, valide son
    contenu et décompresse l'archive.

    Args:
        year: l'année ciblée
        month: le mois ciblé
    """
    url = f"{settings.GITLAB_API_URL}/jobs/artifacts/main/download?job=update-db"
    response = requests.get(
        url, headers={"PRIVATE-TOKEN": settings.GITLAB_TOKEN}, timeout=60
    )
    with ZipFile(response.raw) as update:  # type: ignore
        validate_update(update.namelist(), year, month)
        update.extractall(settings.BASE_DIR)


def load_update(year: str, month: str) -> None:
    """Chargement des données pour `year`/`month`."""
    call_command("load", year=year, month=month)


def delete_update(job_id: int) -> None:
    """Suppression des artefacts pour le job `job_id`."""
    url = f"{settings.GITLAB_API_URL}/jobs/{job_id}/artifacts"
    requests.delete(url, headers={"PRIVATE-TOKEN": settings.GITLAB_TOKEN}, timeout=60)
