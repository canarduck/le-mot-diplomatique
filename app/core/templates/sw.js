const offlineUrl = "offline.html";
const appVersion = "{% load motdiplo %}{% version %}";
const cacheName = appVersion

const appShellFiles = [
    '/static/css/style.css?v{% version %}',
    '/static/favicon.ico?v{% version %}',
    '/static/logo.svg?v{% version %}',
    '/static/js/htmx.min.js?v{% version %}',
    '/static/js/chart.min.js?v{% version %}',
];


self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(cacheName).then((cache) => {
            cache.add(new Request(offlineUrl, { cache: "reload" }));
            return cache.addAll(appShellFiles);
        })
    );
    self.skipWaiting();
});

self.addEventListener("activate", (event) => {
    event.waitUntil(
        self.registration.navigationPreload.enable()
    );
    self.clients.claim();
});