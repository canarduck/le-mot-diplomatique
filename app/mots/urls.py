"""Routeur."""

from analyzer.views import (
    NewWordsFeed,
    alphabetical_index,
    article,
    bogus,
    home,
    letter,
    search,
    word,
    year,
    year_index,
)
from core.views import (
    deploy_webhook,
    humans_txt,
    offline,
    robots_txt,
    security_txt,
    sw_js,
    update_webhook,
)
from django.conf import settings
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("", home, name="home"),
    path("robots.txt", robots_txt, name="robots"),
    path("humans.txt", humans_txt, name="humans"),
    path(".well-known/security.txt", security_txt, name="security"),
    path("sw.js", sw_js, name="sw"),
    path("offline.html", offline, name="offline"),
    path("rss/", NewWordsFeed(), name="rss"),
    path("lettres/", alphabetical_index, name="alphabetical-index"),
    path("lettres/<str:letter>", letter, name="letter"),
    path("années/", year_index, name="year-index"),
    path("années/<int:year>", year, name="year"),
    path("recherche/", search, name="search"),
    path("mot/<str:slug>", word, name="word"),
    path("problématique/<str:slug>", bogus, name="bogus"),
    path("article/<int:id>", article, name="article"),
    path(settings.ADMIN_PATH, admin.site.urls),
    path(f"hook/gitlab/{settings.WEBHOOK_DEPLOY_PATH}/", deploy_webhook, name="deploy"),
    path(f"hook/gitlab/{settings.WEBHOOK_UPDATE_PATH}/", update_webhook, name="update"),
]

if "silk" in settings.INSTALLED_APPS:
    urlpatterns.append(path("silk/", include("silk.urls", namespace="silk")))
