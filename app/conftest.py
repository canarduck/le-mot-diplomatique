"""Fixtures pytest."""

import pytest
from pytest_factoryboy import register
from tests.factories import ArticleFactory, LemmaFactory, WordFactory

register(ArticleFactory)
register(WordFactory)
register(LemmaFactory)


@pytest.fixture(autouse=True)
def _template_debug(settings):
    """Requis par django-coverage-plugin."""
    settings.TEMPLATES[0]["OPTIONS"]["debug"] = True


@pytest.fixture(autouse=True)
def _dummy_cache(settings):
    """À ce stade pas de tests avec le cache actif."""
    settings.CACHES = {
        "default": {
            "BACKEND": "django.core.cache.backends.dummy.DummyCache",
        }
    }


@pytest.fixture(autouse=True)
def _test_environment(settings):
    """Environnement de test."""
    settings.ENVIRONMENT = "test"
    settings.SESSION_COOKIE_SECURE = False
    settings.CSRF_COOKIE_SECURE = False
    settings.SECURE_SSL_REDIRECT = False
    settings.SECURE_HSTS_SECONDS = 0


@pytest.fixture
def _debug_mode(settings):
    """Le debug actif."""
    settings.DEBUG = True
