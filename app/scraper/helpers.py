"""Outils de scraping."""

import json
import re
import unicodedata
from datetime import date, datetime
from pathlib import Path

import requests
from bs4 import BeautifulSoup
from requests.cookies import RequestsCookieJar, cookiejar_from_dict

BASE_URL = "https://www.monde-diplomatique.fr"
LOGIN_URL = "https://lecteurs.mondediplo.net?page=connexion_sso"
LOGIN_COOKIES = "cookies.json"


def save_login_cookies(jar: RequestsCookieJar, path: Path) -> None:
    """Conversion en json les cookies d'authentification."""
    file = path / LOGIN_COOKIES
    with file.open("w") as handle:
        json.dump(jar.get_dict(), handle)


def get_login_cookies(path: Path) -> RequestsCookieJar | None:
    """Récupère les cookies d'authentification depuis un fichier json.

    Args:
        path (Path): chemin de base du fichier

    Returns:
        Les cookies ou rien si le fichier n'existe pas ou si PHPSESSID a expiré.
    """
    file = path / LOGIN_COOKIES
    if not file.exists():
        return None
    try:
        with file.open("r") as handle:
            jar = cookiejar_from_dict(json.load(handle))
    except Exception:
        file.unlink()  # json invalide visiblement
        return None

    jar.clear_expired_cookies()
    if jar.get("PHPSESSID") is None:
        file.unlink()  # on delete le cookie qui ne sert plus à rien
        return None
    return jar


def login(email: str, password: str, csrf: str) -> RequestsCookieJar:
    """Authentification sur le site du diplo."""
    # Envoi du frm d'auth
    payload = {
        "page": "connexion_sso",
        "formulaire_action": "identification_sso",
        "formulaire_action_args": csrf,
        "retour": BASE_URL,
        "site_distant": BASE_URL,
        "email": email,
        "mot_de_passe": password,
        "session_remember": "oui",
        "valider": "Valider",
    }
    # qq chose là dedans est requis
    headers = {
        "User-Agent": "LMD mots",
        "Accept": "text/html",
        "Accept-Language": "fr-FR",
        "Referer": "https://www.monde-diplomatique.fr/",
        "Content-Type": "application/x-www-form-urlencoded",
        "Origin": "https://www.monde-diplomatique.fr",
        "DNT": "1",
    }
    response = requests.post(LOGIN_URL, data=payload, headers=headers, timeout=20)
    response.raise_for_status()
    return response.cookies


def find_articles_links(year: str, month: str) -> list[str]:
    """Retourne les urls des articles pour le numéro (année/mois) demandé.

    Args:
        year: année
        month: mois

    Returns:
        Liste d'url
    """
    path = f"/{year}/{month}/"
    url = f"{BASE_URL}{path}"
    response = requests.get(url, timeout=5)
    response.raise_for_status()
    soup = BeautifulSoup(response.text, "lxml")
    articles = soup.find("div", class_="contenu-principal")
    # breakpoint()
    if not articles:  # pas d'article pour `year`/`month` (ex. 1966/09)
        return []
    return [
        BASE_URL + anchor["href"]
        for anchor in articles.select(f'a[href^="{path}"]')
        if "#" not in anchor["href"]
    ]


def save_article(url: str, cookies: RequestsCookieJar, path: Path) -> None:
    """Enregistre la page `url` dans `path`.

    Utilise les cookies récupérés par `login` pour récupérer l'article complet.
    """
    # ex. https://www.monde-diplomatique.fr/2022/01/RAMIREZ/64240
    article_id = url.split("/")[-1]
    response = requests.get(url, cookies=cookies, timeout=30)
    response.raise_for_status()
    with open(path / f"{article_id}.html", mode="wb") as file:
        file.write(response.content)


def get_soup(page: str) -> BeautifulSoup:
    """Une instance de BeautifulSoup pour `page` utilisant `lxml`."""
    return BeautifulSoup(page, "lxml")


def get_url(soup: BeautifulSoup) -> str:
    """Url de la page depuis les meta tags."""
    return soup.find("meta", property="og:url")["content"]


def get_id(url: str) -> int:
    """Id de l'article depuis l'url."""
    return int(url.split("/")[-1])


def get_title(soup: BeautifulSoup) -> str:
    """Retourne title + auteur, sans numéro du diplo.

    ex. full_title: Titre, par x (Le Monde diplomatique, mois 1999)
    """
    return soup.title.get_text().rsplit("(")[0].strip()


def get_published_on(soup: BeautifulSoup) -> date:
    """Date de publication depuis les meta tags."""
    published_time = soup.find("meta", property="article:published_time")["content"]
    return datetime.strptime(published_time, "%Y-%m-%d").date()


def get_content(soup: BeautifulSoup) -> str:
    """Corps de l'article.

    Args:
        soup: la page

    Returns:
        le chapo et les paragraphes.
    """
    main_content = soup.find("div", class_="contenu-principal")
    paragraphs: list[str] = []
    paragraphs += get_paragraphs(main_content.find("div", class_="chapo"))
    paragraphs += get_paragraphs(main_content.find("div", class_="texte"))
    return "\n\n".join(paragraphs)


def handle_special_cases(article_id: int, content: str) -> str:
    """Gestion des cas particuliers.

    Retourne un `content` modifié si besoin.

    * 59366 : article en plusieurs langues, on ne garde que le FR.
    """
    if article_id == 59366:
        # on tronque avant les traductions
        return content[: content.index("Retrouvez ci-dessous")]
    return content


def get_paragraphs(soup: BeautifulSoup) -> list[str]:
    """Retourne le contenu textuel des `<p>` de `soup`.

    Les sauts de lignes sont supprimés dans les paragraphes.

    Args:
        soup: le container de l'article

    Returns:
        les contenus textuels des paragraphes.
    """
    if not soup:
        return []
    return [sanitize(paragraph.get_text()) for paragraph in soup.find_all("p")]


def sanitize(text: str) -> str:
    """Nettoyage d'un paragraphe avant indexation."""
    methods = [
        "normalize_utf8",
        "cleanup_line_and_spaces",
        "cleanup_dashes",
        "cleanup_quotes_and_parenthesis",
        "cleanup_m_dot",
        "cleanup_dashed_initialism",
        "cleanup_dashed_quotes",
        "cleanup_dotted_acronyms",
        "cleanup_urls",
        "cleanup_slashes",
        "cleanup_commas",
        "cleanup_parenthesis",
        "replace_punctuation",
    ]
    for method in methods:
        text = globals()[method](text)

    return text


def deduplicate_spaces(text: str) -> str:
    """Dédoublonnage des espaces."""
    return " ".join(text.split())


def normalize_utf8(text: str) -> str:
    r"""Normalisation utf8.

    BeautifulSoup encode parfois les entités html (&nbsp;) en utf8 (\xa0) qu'on
    normalise ici avec unicodedata.
    """
    return unicodedata.normalize("NFKD", text)


def cleanup_line_and_spaces(text: str) -> str:
    """Nettoyage des sauts de lignes & espaces.

    * suppression des sauts de lignes
    * suppression des espaces en double, triple, etc.
    """
    text = " ".join(text.splitlines())
    return deduplicate_spaces(text)


def cleanup_dashes(text: str) -> str:
    """Tirets isolés."""
    pattern = r"\s-(?P<word>\w)"
    substitution = "-\\g<word>"
    return re.sub(pattern, substitution, text, flags=re.MULTILINE)


def cleanup_quotes_and_parenthesis(text: str) -> str:
    """Espaces avant et après guillemets.

    Ex: adolescent(e -> adolescent ( e
    """
    for sign in ['"', "«", "»", "("]:
        text = text.replace(sign, f" {sign} ")
    return deduplicate_spaces(text)


def cleanup_m_dot(text: str) -> str:
    """Plein de M.Xxx dans les articles, on ajoute un espace."""
    pattern = r"M\.(?P<lastname>[A-Z].*?\b)"
    substitution = "M. \\g<lastname>"
    return re.sub(pattern, substitution, text, flags=re.MULTILINE)


def cleanup_dashed_initialism(text: str) -> str:
    """Ajout d'espaces autour du tiret qui rassemble deux sigles ou un sigle + un mot.

    Ex. U.R.S.S.-États-Unis -> U.R.S.S. - États-Unis
    Ex. a.-bristol -> a. - bristol
    """
    text = text.replace(".-", ". - ")
    return deduplicate_spaces(text)


def cleanup_dashed_quotes(text: str) -> str:
    """Nettoyage des mots valises faits avec des guillemets.

    Ex. anti-"Cosby Show" -> anti - " Cosby Show"
    """
    text = text.replace('-"', ' - " ')
    return deduplicate_spaces(text)


def cleanup_dotted_acronyms(text: str) -> str:
    """On retire les points dans les sigles.

    À passer après `cleanup_dashed_initialism` pour commencer par séparer les sigles.

    ex. éviter deux entrées distinctes pour  C.N.R.S. (en 75) et CNRS (en 84)
    """
    pattern = r"(?P<letter>[A-Z])\.(?![A-Z]{2})"
    substitution = "\\g<letter>"
    return re.sub(pattern, substitution, text, flags=re.MULTILINE)


def cleanup_urls(text: str) -> str:
    """Séparation des urls collées au texte.

    Ex. européennehttp://ue.eu.int -> européenne http://ue.eu.int
    """
    text = text.replace("http", " http")
    return deduplicate_spaces(text)


def cleanup_slashes(text: str) -> str:
    """On isole les mots avec des barres obliques.

    Ne pas passer avant `remove_urls`

    Ex. aîné/cadet -> aîné / cadet
    """
    text = text.replace("/", " / ")
    return deduplicate_spaces(text)


def cleanup_commas(text: str) -> str:
    """Ajout d'un espace après les virgules.

    Ex. partisan,ce site -> partisan, ce site
    """
    pattern = r",(?=[^\s])"
    substitution = ", "
    return re.sub(pattern, substitution, text, flags=re.MULTILINE)


def cleanup_parenthesis(text: str) -> str:
    """Espaces avant les parenthèses.

    Ex: adolescent(e -> adolescent ( e
    """
    for sign in ['"', "«", "»", "("]:
        text = text.replace(sign, f" {sign} ")
    return deduplicate_spaces(text)


def replace_punctuation(text: str) -> str:
    """Ponctuation."""
    replacements = [("...", "…"), (" ’", "")]
    for replacement in replacements:
        text = text.replace(replacement[0], replacement[1])
    return text
