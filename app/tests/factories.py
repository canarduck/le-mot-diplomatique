import factory
from analyzer.models import Article, Lemma, Word
from faker import Faker

faker = Faker(locale="fr")


class ArticleFactory(factory.django.DjangoModelFactory):
    title = factory.Faker("sentence", locale="fr")
    published_on = factory.Faker("date_this_decade")

    class Meta:
        model = Article

    @factory.sequence
    def id(number) -> int:
        """Oui, id n'est pas un autofield."""
        return number

    @factory.sequence
    def url(number) -> str:
        return f"{faker.url()}{number}"


class LemmaFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Lemma
        skip_postgeneration_save = True

    @factory.sequence
    def text(number) -> str:
        word = faker.word()
        return f"{word}_{number}"

    @factory.post_generation
    def words(self, create: bool, extracted: list[Word], **kwargs) -> None:
        if not create:
            return

        if extracted:
            self.words.add(*extracted)


class WordFactory(factory.django.DjangoModelFactory):
    first_used_in = factory.SubFactory(ArticleFactory)
    lemma = factory.SubFactory(LemmaFactory)

    class Meta:
        model = Word
        skip_postgeneration_save = True

    @factory.sequence
    def text(number) -> str:
        word = faker.word()
        return f"{word}_{number}"

    @factory.post_generation
    def articles(self, create: bool, extracted: list[Article], **kwargs) -> None:
        if not create:
            return
        if self.first_used_in:
            self.articles.add(self.first_used_in)
        if extracted:
            self.articles.add(*extracted)
