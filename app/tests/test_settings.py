import os
from datetime import datetime

from mots.settings import CALVER_FORMAT, version


class TestVersion:
    def test_git(self, tmp_path):
        modified = datetime(2022, 1, 2, 3, 4)
        dot_git = tmp_path / ".git"
        dot_git.mkdir()
        os.utime(dot_git, (modified.timestamp(), modified.timestamp()))
        assert version(tmp_path, datetime.now()) == "22.1.2.304"

    def test_fallback(self, tmp_path):
        now = datetime.now()
        dot_git = tmp_path / ".git"
        assert not dot_git.exists()
        assert version(tmp_path, now) == now.strftime(CALVER_FORMAT)
