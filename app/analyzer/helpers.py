"""Analyse des articles et extraction des mots."""

import re
from typing import TYPE_CHECKING

from .models import Word

if TYPE_CHECKING:
    from spacy import Language
    from spacy.tokens import Token

ROMAN_NUMBER_FLAG_ID = 63
MIN_TOKEN_LENGTH = 2


def is_a_roman_number(text: str) -> bool:
    """Drapeau pour identifier les chiffres romains parmi les jetons."""
    numbers = [
        "I",
        "II",
        "III",
        "IV",
        "V",
        "VI",
        "VII",
        "VIII",
        "IX",
        "X",
        "XI",
        "XII",
        "XIII",
        "XIV",
        "XV",
        "XVI",
        "XVII",
        "XVIII",
        "XIX",
        "XX",
        "XXI",
        "XXII",
    ]
    numbers = numbers + [n + "e" for n in numbers]
    return text in numbers


def tokenize(nlp: "Language", text: str) -> list["Token"]:
    """Retourne les mots _utiles_ d'un texte."""
    doc = nlp(text)
    return [token for token in doc if filter_token(token)]


def filter_token(token: "Token") -> bool:
    """Retourne vrai si le jeton est utile."""
    if (
        len(token.text) < MIN_TOKEN_LENGTH
        or token.is_punct
        or token.is_space
        or token.is_stop
        or token.is_digit
        or token.like_num
        or token.like_email
        or token.like_url
    ):
        return False
    if ")" in token.text or "/" in token.text:  # pas de 1)de ou b./pd
        return False
    # pas de mot qui termine par autre chose qu'une lettre ou de chiffre en premier
    # caractère
    if not token.text[-1].isalpha() or not token.text[0].isalpha():
        return False
    # pas de chiffres romains
    if token.check_flag(ROMAN_NUMBER_FLAG_ID):  # type: ignore
        return False
    # au moins 50% de lettres dans le mot
    if (  # noqa: SIM103
        len([c for c in token.text if c.isalpha()]) <= len(token.text) / 2
    ):
        return False
    return True


def suffix_search_query(query: str) -> str:
    """Ajout d'un joker à la recherche si besoin.

    Il est possible d'utiliser ^ et * sur la recherche FTS, par défaut on ajoute un
    suffixe `*` sauf s'il est déjà présent ou que la chaîne commence par `^`.

    On isole la chaîne recherchée du préfixe et suffixe avec des guillemets pour éviter
    les erreur de syntaxe FTS sur les `:`, les `/`, etc.

    Exemples:
        train -> "train"*
        ^tra* -> ^"tra"*
        train: -> "train:"*

    Args:
        query: la chaîne envoyée

    Returns:
        la chaîne avec * (ou non)
    """
    prefix = suffix = ""
    if query.startswith("^"):
        prefix = "^"
        query = query[1:]
    if query.endswith("*"):
        suffix = "*"
        query = query[:-1]

    # par défaut on ajoute un joker de fin de chaîne
    if not prefix:
        suffix = "*"

    return f'{prefix}"{query}"{suffix}'


def find_word_in_article(word: Word) -> str | None:
    """Retourne la phrase contenant `word` dans le premier article.

    Args:
        word: le mot recherché

    Returns:
        une phrase ou None
    """
    pattern = r"[^.?!]*(?<=[’.?\s])WORD(?=[\s.?!,;-])[^.?!]*[.?!]"
    pattern = pattern.replace("WORD", word.text)
    try:
        content = word.first_used_in.get_txt_path().read_text()
        match = re.search(pattern, content, flags=re.MULTILINE | re.IGNORECASE)
    except Exception:
        import logging

        logger = logging.getLogger(__name__)
        logger.exception(f"Erreur dans la recherche d'un extrait pour « {word} »")
        return None

    return match.group(0).strip() if match else None
