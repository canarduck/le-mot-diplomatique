from datetime import datetime
from http import HTTPStatus
from unittest.mock import Mock, patch

import pytest
from django.urls import reverse
from django.utils.formats import date_format
from pytest_django.asserts import (
    assertContains,
    assertNotContains,
    assertTemplateNotUsed,
    assertTemplateUsed,
)

from analyzer.models import Article, Word
from analyzer.views import NewWordsFeed

from .test_models import rebuild_fts

pytestmark = pytest.mark.django_db


class TestHome:
    @pytest.fixture(autouse=True)
    def _url(self, client):
        self.url = reverse("home")
        self.client = client

    def get_response(self, **kwargs):
        return self.client.get(self.url, **kwargs)

    def test_status(self, word):
        response = self.get_response()
        assert response.status_code == HTTPStatus.OK

    def test_template(self, word):
        with assertTemplateUsed("home.html"):
            self.get_response()

    def test_content(self, word):
        with assertTemplateUsed("home.html"):
            response = self.get_response()
        assertContains(response, "/sw.js")
        assertContains(response, "/htmx")

    def test_last_article(self, article_factory):
        article_factory.create_batch(10)
        response = self.get_response()
        last_article = Article.objects.last()
        assert response.context["last_article"] == last_article
        formatted_date = date_format(last_article.published_on, "F Y")
        assertContains(response, formatted_date, html=False)

    def test_count_words(self, word_factory):
        word_factory.create_batch(10)
        response = self.get_response()
        assert response.context["count_words"] == 10
        assertContains(response, "10 mots", html=False)

    def test_headers(self, word):
        response = self.get_response()
        assert response.headers["Vary"] == "HX-Request"

    def test_context(self, word):
        response = self.get_response()

        assert "index" in response.context
        assert "new_words" in response.context
        assert "last_article" in response.context
        assert "count_words" in response.context
        assert "random_words" in response.context

    def test_htmx_context(self):
        response = self.get_response(HTTP_HX_REQUEST="true")

        assert "index" not in response.context
        assert "new_words" not in response.context
        assert "last_article" not in response.context
        assert "count_words" not in response.context
        assert "random_words" not in response.context


class TestWord:
    @pytest.fixture(autouse=True)
    def _client(self, client, word_factory, article_factory, settings, tmp_path):
        """Un mot qui appartient à un article de 54 et à 5 plus récents."""
        self.client = client
        self.article = article_factory(published_on=datetime(1954, 1, 1))
        self.articles = article_factory.create_batch(5)
        self.word = word_factory(first_used_in=self.article, articles=self.articles)
        settings.ARCHIVES_DIR = tmp_path
        self.article.get_archive_path().mkdir(parents=True, exist_ok=True)
        self.article.get_txt_path().write_text(
            f"Moi la. Fille aveugle, je vais {self.word}. Les remparts."
        )

    def get_response(self, **kwargs):
        return self.client.get(self.word.get_absolute_url(), **kwargs)

    def test_status(self):
        response = self.get_response()
        assert response.status_code == HTTPStatus.OK

    def test_headers(self):
        response = self.get_response()
        # À cause du bouton bogus qui n'apparaît que pour les admins on a aussi un
        # Vary: Cookie ici
        assert response.headers["Vary"] == "HX-Request, Cookie"

    def test_template(self):
        response = self.get_response()
        assertTemplateUsed(response, "word.html")
        assertTemplateUsed(response, "_word.html")
        assertContains(response, "/sw.js")
        assertContains(response, "/htmx")

    def test_htmx_template(self):
        response = self.get_response(HTTP_HX_REQUEST="true")
        assertTemplateNotUsed(response, "word.html")
        assertTemplateUsed(response, "_word.html")

    def test_not_found(self):
        response = self.client.get(self.word.get_absolute_url() + "abc")
        assert response.status_code == HTTPStatus.NOT_FOUND

    def test_title(self):
        response = self.get_response()
        assertContains(
            response, f"<title>Le Mot Diplomatique : {self.word}</title>", html=True
        )

    def test_first_used_on(self):
        response = self.get_response()
        formatted_date = date_format(self.word.first_used_on, "F Y")
        assertContains(response, formatted_date)

    def test_first_used_in(self):
        response = self.get_response()
        article = self.word.first_used_in
        assertContains(response, str(article), html=True)

    def test_no_related_words(self):
        response = self.get_response()
        assertNotContains(response, 'id="related-words"')

    def test_no_appearances(self, word_factory):
        self.word = word_factory()
        response = self.get_response()
        assertNotContains(response, 'id="appearances"')

    def test_appearances(self, article_factory, word_factory):
        response = self.get_response()
        assertContains(response, 'id="appearances"')
        for article in self.articles:
            assertContains(response, str(article), html=True, count=1)

    def test_related_words(self, word_factory):
        words = word_factory.create_batch(3, lemma=self.word.lemma)
        response = self.get_response()
        assertContains(response, 'id="related-words"')
        for related_word in words:
            assertContains(response, str(related_word))

    def test_excerpt(self):
        response = self.get_response()
        assertContains(response, f'blockquote cite="{self.article.url}"')
        assertContains(response, f"Fille aveugle, je vais {self.word}.")


class TestAlphabeticalIndex:
    @pytest.fixture(autouse=True)
    def _client(self, client, word):
        self.client = client
        self.word = word
        self.article = self.word.first_used_in

    def get_response(self, **kwargs):
        return self.client.get("/lettres/", **kwargs)

    def test_cache(self):
        with patch("analyzer.views.alphabetical_index_cache") as mock_cache:
            self.get_response()
            mock_cache.assert_called()

    def test_title(self):
        response = self.get_response()
        assertContains(
            response, "<title>Index alphabétique du Mot Diplomatique</title>", html=True
        )

    def test_context(self):
        response = self.get_response()
        assert "index" in response.context
        index = Word.non_bogus.alphabetical_index()
        assert list(response.context["index"]) == list(index)


class TestYearIndex:
    @pytest.fixture(autouse=True)
    def _client(self, client, word):
        self.client = client
        self.word = word
        self.article = self.word.first_used_in

    def get_response(self, **kwargs):
        return self.client.get("/années/", **kwargs)

    def test_cache(self):
        with patch("analyzer.views.year_index_cache") as mock_cache:
            self.get_response()
            mock_cache.assert_called()

    def test_title(self):
        response = self.get_response()
        assertContains(
            response, "<title>Index annuel du Mot Diplomatique</title>", html=True
        )

    def test_context(self):
        response = self.get_response()
        assert "index" in response.context
        index = Word.non_bogus.year_index()
        assert list(response.context["index"]) == list(index)


class TestBogus:
    @pytest.fixture(autouse=True)
    def _admin_client(self, admin_client, word):
        self.client = admin_client
        self.word = word

    def get_response(self):
        return self.client.post(
            reverse("bogus", args=[self.word.slug]), HTTP_HX_REQUEST="true"
        )

    def test_anonymous(self, client):
        response = client.post(
            reverse("bogus", args=[self.word.slug]), HTTP_HX_REQUEST="true"
        )
        assert response.status_code == HTTPStatus.FORBIDDEN

    def test_admin_get(self):
        response = self.client.get(
            reverse("bogus", args=[self.word.slug]), HTTP_HX_REQUEST="true"
        )
        assert response.status_code == HTTPStatus.METHOD_NOT_ALLOWED

    def test_admin_post_htmx(self):
        response = self.client.post(
            reverse("bogus", args=[self.word.slug]), HTTP_HX_REQUEST="true"
        )
        assert response.status_code == HTTPStatus.OK

    def test_admin_post_not_htmx(self):
        response = self.client.post(reverse("bogus", args=[self.word.slug]))
        assert response.status_code == HTTPStatus.BAD_REQUEST

    def test_template(self):
        with assertTemplateUsed("_bogus.html"):
            self.get_response()

    def test_content_after_bogus_switch(self, word_factory):
        self.word = word_factory(is_bogus=True)
        response = self.get_response()
        assertContains(response, "button danger")

    def test_content_after_not_bogus_switch(self, word_factory):
        self.word = word_factory(is_bogus=False)
        response = self.get_response()
        assertContains(response, "button success")

    def test_context(self):
        response = self.get_response()
        assert "word" in response.context


class TestArticle:
    @pytest.fixture(autouse=True)
    def _client(self, client, article_factory):
        """Un mot qui appartient à un article de 54 et à 5 plus récents."""
        self.client = client
        self.article = article_factory(published_on=datetime(1954, 1, 1))

    def get_response(self, **kwargs):
        return self.client.get(self.article.get_absolute_url(), **kwargs)

    def test_status(self):
        response = self.get_response()
        assert response.status_code == HTTPStatus.OK

    def test_template(self):
        response = self.get_response()
        assertTemplateUsed(response, "article.html")
        assertTemplateUsed(response, "_article.html")
        assertContains(response, "/sw.js")
        assertContains(response, "/htmx")

    def test_htmx_template(self):
        response = self.get_response(HTTP_HX_REQUEST="true")
        assertTemplateNotUsed(response, "article.html")
        assertTemplateUsed(response, "_article.html")

    def test_title(self):
        response = self.get_response()
        assertContains(
            response, f"<title>Le Mot Diplomatique : {self.article}</title>", html=True
        )

    def test_not_found(self):
        with patch("analyzer.views.cache") as mock_cache:
            mock_cache.get_or_set = Mock(return_value=None)
            response = self.get_response()
        mock_cache.delete.assert_called_once()
        assert response.status_code == HTTPStatus.NOT_FOUND

    def test_context(self):
        response = self.get_response()
        assert "article" in response.context

    def test_headers(self):
        response = self.get_response()
        assert response.headers["Vary"] == "HX-Request"


class TestNewWordsFeed:
    def test_item_title(self, word):
        feed = NewWordsFeed()
        assert feed.item_title(word) == str(word)

    def test_item_description(self, word):
        feed = NewWordsFeed()
        description = feed.item_description(word)
        assert str(word.first_used_in) in description
        formatted_date = date_format(word.first_used_on, "d/m/Y")
        assert formatted_date in description

    def test_items(self, word):
        with patch("analyzer.views.new_words_by_date_cache", return_value=["a", "b"]):
            feed = NewWordsFeed()
            assert feed.items() == ["a", "b"]


class TestLetter:
    @pytest.fixture(autouse=True)
    def _client(self, client, word_factory):
        self.client = client
        self.word = word_factory(text="Allo")
        self.url = "/lettres/a"

    def get_response(self, **kwargs):
        return self.client.get(self.url, **kwargs)

    def test_cache_called(self):
        with patch("analyzer.views.cache") as mock_cache:
            self.get_response()
            mock_cache.get.assert_called_once_with("letter_a_1")

    def test_title(self):
        response = self.get_response()
        assertContains(
            response,
            "<title>Mots en «a» du Mot Diplomatique, page 1</title>",
            html=True,
        )

    def test_headers(self):
        response = self.get_response()
        assert response.headers["Vary"] == "HX-Request"

    def test_context(self):
        response = self.get_response()
        assert "index" in response.context

    def test_context_htmx(self):
        response = self.get_response(HTTP_HX_REQUEST="true")
        assert "index" not in response.context

    def test_context_paginator(self):
        response = self.get_response()
        assert "paginator" in response.context
        assert self.word in response.context["paginator"]

    def test_template(self):
        response = self.get_response()
        assertTemplateUsed(response, "alphabetical_index.html")
        assertTemplateUsed(response, "_list.html")

    def test_htmx_template(self):
        response = self.get_response(HTTP_HX_REQUEST="true")
        assertTemplateNotUsed(response, "alphabetical_index.html")
        assertTemplateUsed(response, "_list.html")


class TestYear:
    @pytest.fixture(autouse=True)
    def _client(self, client, word_factory, article_factory):
        self.client = client
        article = article_factory(published_on=datetime(2022, 1, 1))
        self.word = word_factory(text="Allo", first_used_in=article)
        self.url = "/années/2022"

    def get_response(self, **kwargs):
        return self.client.get(self.url, **kwargs)

    def test_cache_called(self):
        with patch("analyzer.views.cache") as mock_cache:
            self.get_response()
            mock_cache.get.assert_called_once_with("year_2022_1")

    def test_title(self):
        response = self.get_response()
        assertContains(
            response,
            "<title>Mots de 2022 du Mot Diplomatique, page 1</title>",
            html=True,
        )

    def test_headers(self):
        response = self.get_response()
        assert response.headers["Vary"] == "HX-Request"

    def test_context(self):
        response = self.get_response()
        assert "index" in response.context

    def test_context_htmx(self):
        response = self.get_response(HTTP_HX_REQUEST="true")
        assert "index" not in response.context

    def test_context_paginator(self):
        response = self.get_response()
        assert "paginator" in response.context
        assert self.word in response.context["paginator"]

    def test_template(self):
        response = self.get_response()
        assertTemplateUsed(response, "year_index.html")
        assertTemplateUsed(response, "_list.html")

    def test_htmx_template(self):
        response = self.get_response(HTTP_HX_REQUEST="true")
        assertTemplateNotUsed(response, "year_index.html")
        assertTemplateUsed(response, "_list.html")


class TestSearch:
    @pytest.fixture(autouse=True)
    def _client(self, client, word_factory):
        self.client = client
        self.word = word_factory(text="Allo")
        rebuild_fts()
        self.url = "/recherche/"

    def get_response(self, query, **kwargs):
        url = f"{self.url}?query={query}" if query else self.url
        return self.client.get(url, **kwargs)

    def test_headers(self):
        response = self.get_response("all")
        assert response.headers["Vary"] == "HX-Request"

    def test_context_ok(self):
        response = self.get_response("all")
        assert response.context["query"] == "all"
        assert response.context["extra_params"] == "query=all&"
        assert self.word in response.context["paginator"]

    def test_context_no_query(self):
        response = self.get_response("")
        assert response.context["query"] is None
        assert "extra_params" not in response.context
        assert "paginator" not in response.context

    def test_context_query_too_short(self):
        response = self.get_response("yo")
        assert response.context["query"] == "yo"
        assert "extra_params" not in response.context
        assert "paginator" not in response.context

    def test_template(self):
        response = self.get_response("all")
        assertTemplateUsed(response, "search.html")
        assertTemplateUsed(response, "_results.html")

    def test_htmx_template(self):
        response = self.get_response("all", HTTP_HX_REQUEST="true")
        assertTemplateNotUsed(response, "search.html")
        assertTemplateUsed(response, "_results.html")
