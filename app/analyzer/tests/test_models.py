from datetime import datetime
from unittest.mock import patch

import pytest
from django.db import connection

from analyzer.models import RANDOM_WORDS_COUNT, Lemma, Word, WordManager

pytestmark = pytest.mark.django_db


def rebuild_fts() -> None:
    """Reconstruction de l'index FTS.

    Pour une raison qui m'échappe, parfois on se prend des
    `django.db.utils.DatabaseError: database disk image is malformed` quand on tente
    de faire une recherche sur l'index, après avoir insérer un mot.

    Pas réussi à en faire une fixture, car il faut l'exécuter après l'ajout d'un `Word`.
    """
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO word_fts(word_fts) VALUES('rebuild');")
        cursor.fetchone()


class TestArticle:
    def test_str(self, article):
        assert str(article) == article.title

    def test_words_count(self, article, word_factory):
        assert article.words_count == 0
        article.word_set.add(word_factory())
        assert article.words_count == 1


class TestWord:
    def test_str(self, word):
        assert str(word) == word.text

    def test_nav(self, word_factory):
        word3 = word_factory(text="cane")
        word1 = word_factory(text="ane")
        word2 = word_factory(text="banane")
        assert word2.previous.text == word1.text
        assert word3.previous.text == word2.text
        assert word1.following.text == word2.text
        assert word2.following.text == word3.text
        assert word1.previous is None
        assert word3.following is None

    def test_save_sets_slug(self, word_factory):
        word = word_factory(text="test", slug=None)
        assert word.slug == "test"

    def test_save_with_article(self, word, article):
        word.first_used_on = None
        word.articles.add(article)
        word.save()
        assert word.first_used_on == article.published_on

    def test_other_articles(self, word_factory, article_factory):
        word = word_factory()
        assert not word.other_articles

        first = article_factory(published_on=datetime(1954, 1, 1))
        others = article_factory.create_batch(5)
        word = word_factory(first_used_in=first, articles=others)
        assert len(word.other_articles) == 5
        assert first not in word.other_articles

    def test_articles_count(self, word, article_factory):
        assert word.articles_count == 1
        articles = article_factory.create_batch(2)
        word.articles.add(*articles)
        assert word.articles_count == 3

    @pytest.mark.parametrize(
        ("text", "slug"),
        [
            ("mot", "mot"),
            ("m/t", "mt"),
            ("près", "pr%C3%A8s"),
        ],
    )
    def test_get_absolute_url(self, word_factory, text, slug):
        word = word_factory(text=text)
        assert word.get_absolute_url() == f"/mot/{slug}"

    def test_related_words(self, word_factory, lemma):
        words = word_factory.create_batch(2, lemma=lemma)
        first_word = words[0]
        second_word = words[1]
        assert first_word == second_word.related_words.first()
        assert second_word == first_word.related_words.first()

    def test_related_words_exclusion(self, word_factory, lemma):
        words = word_factory.create_batch(3, lemma=lemma)
        word = words[0]
        for word in words:
            assert word.related_words.count() == 2
            assert word not in word.related_words.all()

    def test_no_related_words(self, word_factory):
        words = word_factory.create_batch(2)
        first_word = words[0]
        second_word = words[1]
        assert first_word.related_words.count() == 0
        assert second_word.related_words.count() == 0


class TestLemma:
    def test_str(self, lemma):
        assert str(lemma) == lemma.text


class TestWordManager:
    def test_manager(self):
        assert isinstance(Word.non_bogus, WordManager)

    def test_default_manager(self):
        assert not isinstance(Word.objects, WordManager)

    def test_random(self, word_factory):
        word_factory.create_batch(50)
        assert len(Word.non_bogus.random()) <= RANDOM_WORDS_COUNT

    def test_bogus(self, word_factory):
        word_factory(text="bogus", is_bogus=True)
        assert Word.objects.filter(text="bogus").exists()
        assert not Word.non_bogus.filter(text="bogus").exists()

    def test_search(self, word_factory):
        word1 = word_factory(text="mot")
        word2 = word_factory(text="MOT")
        word3 = word_factory(text="moteur")
        word4 = word_factory(text="motard", is_bogus=True)
        word5 = word_factory(text="mar-mot")
        word6 = word_factory(text="autre")
        word7 = word_factory(text="banane")
        rebuild_fts()

        results = Word.non_bogus.search("mot*")[:10]

        assert len(results) == 4
        assert word1 in results
        assert word2 in results
        assert word3 in results
        assert word4 not in results  # is_bogus
        assert word5 in results
        assert word6 not in results
        assert word7 not in results

    def test_search_initial_and_suffix(self, word_factory):
        word = word_factory(text="prudence")
        rebuild_fts()

        results = Word.non_bogus.search('^"prude"*')[:10]

        assert len(results) == 1
        assert word in results

    def test_starts_with(self, word_factory):
        word1 = word_factory(text="ane")
        word2 = word_factory(text="banane")
        word3 = word_factory(text="cane")
        word4 = word_factory(text="Arbre")
        word5 = word_factory(text="police")
        results = Word.non_bogus.starts_with("a")
        assert len(results) == 2
        assert word1 in results
        assert word2 not in results
        assert word3 not in results
        assert word4 in results
        assert word5 not in results

    def test_from_year(self, word_factory, article_factory):
        article1 = article_factory(published_on=datetime(2022, 4, 2))
        article2 = article_factory(published_on=datetime(2020, 4, 2))

        word1 = word_factory(text="ane", first_used_in=article1, articles=[article2])
        word2 = word_factory(text="banane", first_used_in=article2)

        results = Word.non_bogus.from_year(2022)
        assert len(results) == 1
        assert word1 in results
        assert word2 not in results

    def test_newest(self, article_factory, lemma_factory, word_factory):
        article1 = article_factory(published_on=datetime(2022, 4, 2))
        article2 = article_factory(published_on=datetime(2020, 4, 2))
        lemma1 = lemma_factory()
        lemma2 = lemma_factory()
        lemma3 = lemma_factory()
        # deux mots avec le même lemme, donc pas nouveau
        word1 = word_factory(first_used_in=article1, lemma=lemma1)
        word2 = word_factory(first_used_in=article2, lemma=lemma1)
        # lui est sur un article plus vieux
        word3 = word_factory(first_used_in=article2, lemma=lemma2)
        # lui est bien nouveau
        word4 = word_factory(first_used_in=article1, lemma=lemma3)

        new_words = Word.non_bogus.newest(article1.published_on)

        assert word1 not in new_words
        assert word2 not in new_words
        assert word3 not in new_words
        assert word4 in new_words


class TestWordMergeWith:
    def test_call_delete_unused_lemmas(self, word, word_factory):
        words = word_factory.create_batch(3)
        with patch("analyzer.models.delete_unused_lemmas") as mock:
            word.merge_with(words)
            mock.assert_called_once()

    def test_first_used_in(self, article_factory, word_factory):
        article1 = article_factory(published_on=datetime(2000, 4, 2))
        article2 = article_factory(published_on=datetime(1974, 4, 2))
        word = word_factory(first_used_in=article1)
        words = [word_factory(first_used_in=article2)]
        word.merge_with(words)
        assert word.first_used_in == article2

    def test_articles(self, article, article_factory, word_factory):
        articles = article_factory.create_batch(5)
        word = word_factory(first_used_in=article, articles=[article])
        words = [word_factory(first_used_in=articles[0], articles=articles)]
        word.merge_with(words)
        assert word.articles.count() == 5 + 1
        for article in articles:
            assert article in word.articles.all()

    def test_deletion(self, word, word_factory):
        words = word_factory.create_batch(5)
        texts = [w.text for w in words]
        word.merge_with(words)
        assert not Word.objects.filter(text__in=texts).exists()

    def test_lemma_deletion(self, word, word_factory):
        lemma = word.lemma
        word_same_lemma = word_factory(lemma=lemma)
        words = word_factory.create_batch(4)
        texts = [w.lemma.text for w in words]
        words.append(word_same_lemma)
        word.merge_with(words)
        assert word.lemma == lemma
        assert not Lemma.objects.filter(text__in=texts).exists()
