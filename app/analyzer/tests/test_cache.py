import pytest
from django.core.cache import cache

from analyzer.models import Word
from analyzer.views import alphabetical_index_cache, year_index_cache

pytestmark = pytest.mark.django_db


@pytest.fixture(autouse=True)
def _locmem_cache(settings):
    """Cache en mémoire."""
    settings.CACHES = {
        "default": {
            "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        }
    }
    yield
    cache.clear()


def test_alphabetical_index_cache(django_assert_num_queries, word):
    index = Word.non_bogus.alphabetical_index()
    with django_assert_num_queries(1):
        cold = alphabetical_index_cache()
        hot = alphabetical_index_cache()
    assert list(index) == list(cold) == list(hot)


def test_year_index_cache(django_assert_num_queries, word):
    index = Word.non_bogus.year_index()
    with django_assert_num_queries(1):
        cold = year_index_cache()
        hot = year_index_cache()
    assert list(index) == list(cold) == list(hot)
