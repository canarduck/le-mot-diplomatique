from contextlib import redirect_stderr
from datetime import datetime
from io import StringIO
from unittest.mock import patch

import pytest
from django.core.management import call_command

from analyzer.models import Article


@pytest.fixture
def _archives_dir(settings):
    settings.ARCHIVES_DIR = settings.APP_DIR / "tests" / "archives"


@pytest.mark.django_db
@pytest.mark.usefixtures("_archives_dir")
class TestLoad:
    def test_no_articles_dot_csv(self):
        out = StringIO()
        with patch("analyzer.management.commands.load.DictReader") as mock_reader:
            call_command("load", year="2022", month="04", stdout=out)
            mock_reader.assert_not_called()
        assert "Terminé" in out.getvalue()

    def test_article_created(self):
        with pytest.raises(Article.DoesNotExist):
            Article.objects.get(pk=21220)

        err = StringIO()
        with redirect_stderr(err):
            call_command("load", year="2022", month="03")
        assert "Meow" in err.getvalue()

        article = Article.objects.get(pk=21220)
        assert article.title == "Meow ?, par George Michel"
        assert article.url == "https://mot-diplo.fr/2022/03/MICHEL/21220"
        assert article.published_on == datetime(2022, 3, 1).date()

    def test_handle_article_id_not_numeric(self):
        out = StringIO()
        call_command("load", year="2022", month="03", stdout=out)
        assert "Terminé" in out.getvalue()
        assert Article.objects.count() == 1
