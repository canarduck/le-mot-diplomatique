from dataclasses import dataclass

import pytest

from analyzer.helpers import (
    filter_token,
    find_word_in_article,
    is_a_roman_number,
    suffix_search_query,
)


@pytest.mark.parametrize(
    ("value", "expected"),
    [("I", True), ("V", True), ("i", False), ("XVIIIe", True)],
)
def test_is_a_roman_number(value, expected):
    assert is_a_roman_number(value) == expected


@dataclass
class Token:
    """Simili jeton spaCy."""

    text: str
    is_punct: bool = False
    is_space: bool = False
    is_stop: bool = False
    is_digit: bool = False
    like_num: bool = False
    like_url: bool = False
    like_email: bool = False

    def check_flag(self, flag) -> float:
        return is_a_roman_number(self.text)


class TestFilterToken:
    @pytest.mark.parametrize(
        "attribute",
        [
            "is_punct",
            "is_space",
            "is_stop",
            "is_digit",
            "like_num",
            "like_url",
            "like_email",
        ],
    )
    def test_filtered_attributes(self, attribute):
        token = Token("testicule", **{attribute: True})
        assert not filter_token(token)  # type: ignore

    def test_is_too_short(self):
        token = Token("t")
        assert not filter_token(token)  # type: ignore

    @pytest.mark.parametrize(
        "char",
        [")", "/"],
    )
    def test_char_not_allowed(self, char):
        token = Token(f"testicule{char}")
        assert not filter_token(token)  # type: ignore

    def test_roman_number(self):
        token = Token("XXII")
        assert not filter_token(token)  # type: ignore

    def test_endswith_number(self):
        token = Token("testicule1")
        assert not filter_token(token)  # type: ignore

    def test_startswith_number(self):
        token = Token("1testicule")
        assert not filter_token(token)  # type: ignore

    @pytest.mark.parametrize(
        ("text", "expected"),
        [
            ("f2000f", False),
            ("f200f", False),
            ("f20f", False),
            ("super123génial", True),
        ],
    )
    def test_letter_ratio(self, text, expected):
        token = Token(text)
        assert filter_token(token) == expected  # type: ignore

    @pytest.mark.parametrize(
        "text",
        ["testicule"],
    )
    def test_valid(self, text):
        token = Token(text)
        assert filter_token(token)  # type: ignore


@pytest.mark.parametrize(
    ("query", "expected"),
    [
        ("query", '"query"*'),
        ("query*", '"query"*'),
        ("^query", '^"query"'),
        ("^query*", '^"query"*'),
    ],
)
def test_suffix_search_query(query, expected):
    assert suffix_search_query(query) == expected


@pytest.mark.django_db
def test_find_word_in_article(settings, tmp_path, word_factory):
    settings.ARCHIVES_DIR = tmp_path
    word = word_factory(text="aveugle")
    article = word.first_used_in
    article.get_archive_path().mkdir(parents=True, exist_ok=True)
    article.get_txt_path().write_text(
        "Moi la. Fille aveugle, je vais longer. Les remparts."
    )
    assert find_word_in_article(word) == "Fille aveugle, je vais longer."
