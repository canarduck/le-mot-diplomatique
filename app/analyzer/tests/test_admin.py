from datetime import datetime
from unittest.mock import MagicMock, Mock, patch
from urllib.parse import parse_qs, urlparse

import pytest
from django.contrib.admin.sites import AdminSite
from django.contrib.messages import get_messages
from django.contrib.messages.storage.fallback import FallbackStorage

from analyzer.admin import (
    ArticleAdmin,
    LemmaAdmin,
    WordAdmin,
    first_used_in_link,
    new_words_count,
    related_count,
    tweet_new_word_url,
    word_link,
    words_count,
    words_list,
)
from analyzer.models import Article, Lemma, Word


@pytest.fixture
def admin_site():
    return AdminSite()


@pytest.fixture
@pytest.mark.django_db
def admin_request(rf, admin_user, settings):
    request = rf.get(settings.ADMIN_PATH)
    request.user = admin_user
    # https://www.argpar.se/posts/programming/testing-django-admin
    # sinon on se prend des `django.contrib.messages.api.MessageFailure`
    request.session = "session"
    messages = FallbackStorage(request)
    request._messages = messages
    return request


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("published_on", "edition"),
    [
        (datetime(2022, 1, 1), "de janvier 2022"),
        (datetime(2000, 4, 1), "d’avril 2000"),
        (datetime(1999, 8, 1), "d’août 1999"),
        (datetime(2020, 10, 1), "d’octobre 2020"),
    ],
)
def test_tweet_new_word_url(word_factory, article_factory, rf, published_on, edition):
    request = rf.get("/")
    article = article_factory(published_on=published_on)
    word = word_factory(text="absolument", first_used_in=article)
    url = tweet_new_word_url(word, request)
    o = urlparse(url)
    assert o.hostname == "twitter.com"
    assert o.path == "/intent/tweet"

    qs = parse_qs(o.query)
    assert len(qs["text"]) == 1
    text = qs["text"][0]
    assert len(qs["url"]) == 1
    url = qs["url"][0]

    assert "absolument" in text
    assert "@mdiplo" in text
    assert edition in text

    assert url == f"http://testserver{word.get_absolute_url()}"


def test_new_words_count():
    obj = Mock()
    obj.new_words__count = -1
    assert new_words_count(obj) == -1


def test_related_count():
    obj = Mock()
    obj.related_count = -1
    assert related_count(obj) == -1


def test_word_link_bogus():
    obj = Mock()
    obj.is_bogus = True
    assert word_link(obj) == "-"


def test_word_link():
    obj = Mock()
    obj.is_bogus = False
    obj.get_absolute_url = Mock(return_value="word")
    assert word_link(obj) == '<a href="word" target="_blank">Voir</a>'


def test_first_used_in_link():
    obj = Mock()
    article = MagicMock()
    # impossible de savoir ce qu'attend mypy ici...
    article.__str__.return_value = "titre"  # type: ignore[attr-defined]
    article.url = "article"
    obj.first_used_in = article
    assert first_used_in_link(obj) == '<a href="article" target="_blank">titre</a>'


def test_words_count():
    obj = Mock()
    obj.words__count = 10
    assert words_count(obj) == 10


@pytest.mark.django_db
def test_words_list(lemma, word_factory):
    lemma.words.add(word_factory(text="Monde"))
    lemma.words.add(word_factory(text="Diplo"))
    assert words_list(lemma) == "Diplo, Monde"  # order_by text par défaut


class TestArticleAdmin:
    @pytest.fixture(autouse=True)
    def _setup(self, admin_site, admin_request):
        self.admin = ArticleAdmin(Article, admin_site)

    def test_queryset(self, admin_request):
        queryset = self.admin.get_queryset(admin_request)
        assert "new_words__count" in queryset.query.annotations

    def test_list_display(self, admin_request):
        assert new_words_count in self.admin.list_display


class TestWordAdmin:
    @pytest.fixture(autouse=True)
    def _setup(self, admin_site):
        self.admin = WordAdmin(Word, admin_site)

    def test_self_request(self, admin_request):
        assert not getattr(self.admin, "request", False)
        self.admin.get_queryset(admin_request)
        assert self.admin.request == admin_request

    def test_queryset(self, admin_request):
        queryset = self.admin.get_queryset(admin_request)
        assert "related_count" in queryset.query.annotations

    def test_list_display(self):
        list_display = self.admin.list_display
        assert word_link in list_display
        assert first_used_in_link in list_display
        assert related_count in list_display

    @pytest.mark.django_db
    def test_tweet_link_not_bogus(self, admin_request, word):
        word.is_bogus = False
        self.admin.get_queryset(admin_request)
        tweet_link = self.admin.tweet_link(word)
        assert "Tweet" in tweet_link

    @pytest.mark.django_db
    def test_tweet_link_bogus(self, admin_request, word):
        word.is_bogus = True
        self.admin.get_queryset(admin_request)
        assert not self.admin.tweet_link(word)

    @pytest.mark.django_db
    def test_make_bogus(self, admin_request, word_factory):
        bogus_words = word_factory.create_batch(5, is_bogus=False)
        pks = [w.pk for w in bogus_words]
        other_words = word_factory.create_batch(3, is_bogus=False)
        queryset = self.admin.get_queryset(admin_request)
        queryset = queryset.filter(pk__in=pks)
        self.admin.make_bogus(admin_request, queryset)
        for word in Word.objects.filter(pk__in=pks).all():
            assert word.is_bogus
        for word in Word.objects.filter(pk__in=[w.pk for w in other_words]).all():
            assert not word.is_bogus

    @pytest.mark.django_db
    def test_make_bogus_message(self, admin_request, word_factory):
        bogus_words = word_factory.create_batch(2, is_bogus=False)
        queryset = self.admin.get_queryset(admin_request)
        queryset = queryset.filter(pk__in=[w.pk for w in bogus_words])
        self.admin.make_bogus(admin_request, queryset)
        messages = list(get_messages(admin_request))
        assert len(messages) == 1
        assert "2 mots marqués comme problématiques" in messages[0].message

    @pytest.mark.django_db
    def test_make_bogus_clear_cache(self, admin_request, word_factory):
        bogus_words = word_factory.create_batch(2, is_bogus=False)
        queryset = self.admin.get_queryset(admin_request)
        queryset = queryset.filter(pk__in=[w.pk for w in bogus_words])

        with patch("analyzer.admin.cache") as mock:
            self.admin.make_bogus(admin_request, queryset)
            mock.clear.assert_called_once()


class TestLemmaAdmin:
    @pytest.fixture(autouse=True)
    def _setup(self, admin_site):
        self.admin = LemmaAdmin(Lemma, admin_site)

    def test_queryset(self, admin_request):
        queryset = self.admin.get_queryset(admin_request)
        assert "words__count" in queryset.query.annotations

    def test_list_display(self):
        list_display = self.admin.list_display
        assert words_count in list_display
        assert words_list in list_display
