"""Analyse des articles extraits."""

from csv import DictReader, DictWriter

import spacy
from core.management.commands import YearMonthCommand
from django.conf import settings
from django.utils.text import Truncator
from tqdm import tqdm

from analyzer.helpers import ROMAN_NUMBER_FLAG_ID, is_a_roman_number, tokenize

DESC_LENGTH = 25


class Command(YearMonthCommand):
    """Analyse des articles extraits."""

    def pre_loop(self) -> None:
        """Chargement NLP."""
        self.stdout.write("Chargement NLP...")
        self.nlp = spacy.load(settings.NLP_MODEL)
        # ajout du flag pour identifier les chiffres romains
        self.nlp.vocab.add_flag(is_a_roman_number, flag_id=ROMAN_NUMBER_FLAG_ID)
        self.stdout.write(self.style.SUCCESS("OK !"))

    def work_on(self, year: str, month: str) -> None:
        """Exécution de l'analyse."""
        path = settings.ARCHIVES_DIR / year / month
        articles_file = path / "articles.csv"
        if not articles_file.exists():
            return
        with articles_file.open("r") as f:
            reader = DictReader(f)
            articles = list(reader)

        articles_bar = tqdm(articles)
        default_description = f"{month}/{year}".ljust(DESC_LENGTH)
        articles_bar.set_description(default_description)
        for article in articles_bar:
            current_description = Truncator(article["title"]).chars(num=DESC_LENGTH)
            articles_bar.set_description(current_description.ljust(DESC_LENGTH))

            content_file = path / f"{article['id']}.txt"
            csv_file = path / f"{article['id']}.csv"

            if csv_file.exists() and not self.forced:
                continue

            with csv_file.open("w") as f:
                writer = DictWriter(f, ["text", "lemma", "tag", "norm"])
                writer.writeheader()
                for token in tokenize(self.nlp, content_file.read_text()):
                    writer.writerow(
                        {
                            "text": token.text,
                            "lemma": token.lemma_,
                            "tag": token.tag_,
                            "norm": token.norm_,
                        }
                    )
            articles_bar.set_description(default_description)
