"""Migration FTS SQLite."""

from typing import TYPE_CHECKING

from django.db import migrations

if TYPE_CHECKING:
    from django.db.migrations.base import Operation


class Migration(migrations.Migration):
    """Creation d'un index [FTS5][1] pour les mots.

    L'index s'appelle `word_fts` et il réutilise les données de la table word (c'est une
    [External Content Table][2] dans le jargon SQLite).

    Le parseur de jeton unicode par défaut [unicode61][3] est utilisé et on ignore les
    caractères diacritiques lors des recherches.

    Même si on indexe des mots isolés et pas du texte on paramètre le niveau d'
    [informations][4] à `full` car ça nous permet d'utiliser le `^` (« commence par »).

    Une fois l'index créé on le peuple avec la commande [rebuild][5].

    L'index est maintenu à jour grâce à des déclencheurs autour des insertions / mises à
    jour / suppression de mots.

    * [1]: https://www.sqlite.org/fts5.html
    * [2]: https://www.sqlite.org/fts5.html#external_content_tables
    * [3]: https://www.sqlite.org/fts5.html#unicode61_tokenizer
    * [4]: https://www.sqlite.org/fts5.html#the_detail_option
    * [5]: https://www.sqlite.org/fts5.html#the_rebuild_command
    """

    dependencies = [
        ("analyzer", "0001_initial"),
    ]

    operations: list["Operation"] = [
        migrations.RunSQL(
            sql=[
                # Table virtuelle pour l'index
                """CREATE VIRTUAL TABLE IF NOT EXISTS
                    word_fts 
                USING 
                    fts5(
                        text, 
                        content=analyzer_word,
                        tokenize='unicode61 remove_diacritics 2',
                        detail=full
                    );""",
                # Indexation des mots existants
                "INSERT INTO word_fts(word_fts) VALUES('rebuild');",
                # Déclencheurs
                """CREATE TRIGGER tr_word_fts_ai AFTER INSERT ON analyzer_word BEGIN
                    INSERT INTO word_fts(text) VALUES (new.text);
                END;""",
                """CREATE TRIGGER tr_word_fts_ad AFTER DELETE ON analyzer_word BEGIN
                    INSERT INTO word_fts(word_fts, text) VALUES('delete', old.text);
                END;""",
                """CREATE TRIGGER tr_word_fts_au AFTER UPDATE ON analyzer_word BEGIN
                    INSERT INTO word_fts(word_fts, text) VALUES('delete', old.text);
                    INSERT INTO word_fts(text) VALUES (new.text);
                END;""",
            ],
            reverse_sql=[
                "DROP TABLE IF EXISTS word_fts;",
                "DROP TRIGGER IF EXISTS tr_word_fts_ai;",
                "DROP TRIGGER IF EXISTS tr_word_fts_ad;",
                "DROP TRIGGER IF EXISTS tr_word_fts_au;",
            ],
        )
    ]
