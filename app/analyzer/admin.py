"""Admin."""

from urllib.parse import quote

from django.contrib import admin, messages
from django.core.cache import cache
from django.db import models
from django.db.models import QuerySet
from django.http import HttpResponseRedirect
from django.http.request import HttpRequest
from django.shortcuts import render
from django.utils.formats import date_format
from django.utils.html import format_html

from .forms import WordMergeForm
from .models import Article, Lemma, Word


def tweet_new_word_url(word: Word, request: HttpRequest) -> str:
    """Construit une url pour tweeter un nouveau mot.

    cf. https://developer.twitter.com/en/docs/twitter-for-websites/web-intents/overview

    Arguments:
        word: le mot
        request: le contexte de requête django (pour construire l'url absolue du mot)

    Returns:
        Une url qui, une fois cliquée, préremplie le formulaire de tweet avec le mot,
        l'url vers la page et mentionne le monde diplo
    """
    edition = date_format(word.first_used_on, "F Y")
    edition = f"d’{edition}" if edition[0] in ["a", "o"] else f"de {edition}"
    body = f"Nouveau mot dans le @mdiplo {edition} : {word}"
    text = f"text={quote(body)}"
    word_url = request.build_absolute_uri(word.get_absolute_url())
    url = f"url={word_url}"
    twitter = "https://twitter.com/intent/tweet"
    return f"{twitter}?{text}&{url}"


@admin.display(description="Nouveaux mots", ordering="new_words__count")
def new_words_count(obj: Article) -> int:
    """Nombre de nouveaux mots."""
    return obj.new_words__count


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    """Admin des articles."""

    list_display = ["id", "title", "published_on", new_words_count]
    search_fields = ["title"]
    readonly_fields = ["id"]
    date_hierarchy = "published_on"
    view_on_site = True

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        """Annotation nombre de mots."""
        queryset = super().get_queryset(request)
        return queryset.annotate(models.Count("new_words"))


@admin.display(description="Voir")
def word_link(obj: Word) -> str:
    """Lien vers le mot sur le site."""
    if obj.is_bogus:
        return "-"
    return format_html('<a href="{}" target="_blank">Voir</a>', obj.get_absolute_url())


@admin.display(description="Première apparition", ordering="first_used_in")
def first_used_in_link(obj: Word) -> str:
    """Lien vers l’article."""
    article = obj.first_used_in
    return format_html('<a href="{}" target="_blank">{}</a>', article.url, article)


@admin.display(description="Mots liés", ordering="related_count")
def related_count(obj: Word) -> int:
    """Nombre de mots avec le même lemme."""
    return obj.related_count


@admin.register(Word)
class WordAdmin(admin.ModelAdmin):
    """Admin des mots."""

    list_display = [
        "text",
        "lemma",
        "first_used_on",
        first_used_in_link,
        "is_bogus",
        related_count,
        word_link,
        "tweet_link",
    ]
    date_hierarchy = "first_used_on"
    search_fields = ["text"]
    autocomplete_fields = ["first_used_in", "articles", "lemma"]
    view_on_site = True
    actions = ["make_bogus", "merge"]

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        """Passage de request en prop de la vue pour l'utiliser ensuite."""
        self.request = request
        queryset = super().get_queryset(request)
        return queryset.select_related("lemma").annotate(
            related_count=models.Count("lemma__words")
        )

    @admin.display(description="Tweet")
    def tweet_link(self, obj: Word) -> str:
        """Lien pour tweeter le mot."""
        if obj.is_bogus:
            return ""
        return format_html(
            '<a href="{}" target="_blank">Tweet</a>',
            tweet_new_word_url(obj, request=self.request),
        )

    @admin.action(description="Marquer les mots comme problématiques")
    def make_bogus(self, request: HttpRequest, queryset: QuerySet) -> None:
        """Une action de groupe pour désactiver des mots et vidage du cache."""
        count = queryset.update(is_bogus=True)
        cache.clear()
        self.message_user(
            request,
            f"{count} mots marqués comme problématiques, cache vidé.",
            messages.SUCCESS,
        )

    @admin.action(description="Fusionner les mots sélectionnés")
    def merge(self, request: HttpRequest, queryset: QuerySet) -> None:
        """Une action de groupe pour fusionner des mots en doublon."""
        if "apply" in request.POST:
            word = Word.objects.get(text=request.POST["original"])
            words = list(queryset.exclude(text=request.POST["original"]).all())
            word.merge_with(words)
            self.message_user(
                request,
                f"Tous les mots ont été fusionnés dans « {word} »",
                messages.SUCCESS,
            )
            return HttpResponseRedirect(request.get_full_path())

        form = WordMergeForm(
            queryset,
            initial={"_selected_action": queryset.values_list("text", flat=True)},
        )
        return render(request, "admin/word_merge.html", {"form": form})


@admin.display(description="Nbre de mots liés", ordering="words__count")
def words_count(obj: Lemma) -> int:
    """Nombre de mots liés au lemme `obj`."""
    return obj.words__count


@admin.display(description="Mots liés")
def words_list(obj: Lemma) -> str:
    """Liste de mots liés au lemme `obj`."""
    words = [str(word) for word in obj.words.all()]
    return ", ".join(words)


@admin.register(Lemma)
class LemmaAdmin(admin.ModelAdmin):
    """Admin des lemmes."""

    list_display = ["text", words_list, words_count]
    search_fields = ["text"]
    readonly_fields = ["text"]

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        """Annotation nombre de mots."""
        queryset = super().get_queryset(request)
        return queryset.prefetch_related("words").annotate(models.Count("words"))
